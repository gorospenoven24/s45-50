//import { useState } from 'react';
// Proptypes - use to validate props
import PropTypes from 'prop-types'
import { Row, Col, Card } from 'react-bootstrap';
import   {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
	//console.log(props);
	const {_id, name, description, price} = courseProp;
	//  State hook - use to keep track of information related to individual components

	// syntax : const[getter, setter] = useState(initialGetterValue);
	/*const [count,setCount] = useState(0);

	// use to count the seat slots
	const [seat,seatCount] = useState(30);

		function enroll(){
			setCount(count + 1);
			// function to deduct the seats
			seatCount(seat - 1);
			// condition for seats deduction
			if (seat === 0){
				setCount(count);
				seatCount(seat);
				alert('No more seats');
			}
			

		}*/


	return (
		<Row className = "mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
				  <Card.Body>
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle>Description: </Card.Subtitle>
				   <Card.Text>{description}</Card.Text>
				   <Card.Subtitle>Price: </Card.Subtitle>
				   <Card.Text>{price}</Card.Text>
				   <Link className= "btn btn-primary" to ={`/courses/${_id}`}>Details</Link>  
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		)
	}

	// checks the validity of the PropTypes
	CourseCard.propTypes = {
		// "shape" mtehod is to check if the prop object conforms to a sepecific shape
		course: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.string.isRequired
		})
	}