import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login(props) {
        // allows us to comsume the User context object and its properties to use for user validation
        const {user, setUser} = useContext(UserContext)

		// State hooks to store the values of the input fields
		const [email, setEmail] = useState('');
	    const [password, setPassword] = useState('');
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState(false);

	    function authenticate(e) {

	        // Prevents page redirection via form submission
	        e.preventDefault();

            // fetch request to process the backend API
            // Syntax: fetch('url',{options})
            //  .then(res=> res.json()).then(data => {})
            fetch('http://localhost:4000/users/login', {
                method: 'POST',
                headers:{
                    'Content-Type':'application/json'
                },
                body: JSON.stringify({
                    email:email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // if no user information found, the "access" ptroperty will not be valid available and will return undefined
                if(typeof data.access !== "undefined"){
                    // The token will be used to retrieve user information accross the whole frontend application and storing it in the local storage to alw ease of access to the user's information.
                    localStorage.setItem('token', data.access);
                    retrieveUserDeatails(data.access);

                    Swal.fire({
                        title:"Login successfull",
                        icon: "success",
                        text:" Welcome to zuitt!"
                    })
                }
                else{
                    Swal.fire({
                        title:"Authentication Failed!",
                        icon:"error",
                        text: "Check your login details and try again."
                    })
                }

            })

            // set the email of the authenticated user in the local sotrage
            // syntax: localStorage.setItem('propertyname',value);
            /* localStorage.setItem('email', email);*/

            // Set the global user state to have properties obtained from local storage
            /*setUser({
                email: localStorage.getItem('email')
            });*/

	        // Clear input fields after submission
	        setEmail('');
	        setPassword('');

	        // alert(`${email} has been verified! Welcome back!`);

	    }

            const retrieveUserDeatails = (token) =>{
                fetch('http://localhost:4000/users/details',{
                    headers:{
                        Authorization:`Bearer ${ token }`
                    }
                })
                   .then(res => res.json())
                    .then (data => {
                        console.log(data);

                        setUser({
                            id: data._id,
                            isAdmin: data.isAdmin
                      })

                 }) 
            } 


		useEffect(() => {

	        // Validation to enable submit button when all fields are populated and both passwords match
	        if(email !== '' && password !== ''){
	            setIsActive(true);
	        }else{
	            setIsActive(false);
	        }

	    }, [email, password]);


    return (
            (user.id !== null) ?
                <Redirect to="/courses"/>
             :
             <Form onSubmit={(e) => authenticate(e)}>
                 <Form.Group controlId="userEmail">
                     <Form.Label>Email address</Form.Label>
                     <Form.Control 
                         type="email" 
                         placeholder="Enter email" 
                         value={email}
                         onChange={(e) => setEmail(e.target.value)}
                         required
                     />
                 </Form.Group>

                 <Form.Group controlId="password">
                     <Form.Label>Password</Form.Label>
                     <Form.Control 
                         type="password" 
                         placeholder="Password" 
                         value={password}
                         onChange={(e) => setPassword(e.target.value)}
                         required
                     />
                 </Form.Group>
                 { isActive ? 
                     <Button variant="primary" type="submit" id="submitBtn">
                         Submit
                     </Button>
                     : 
                     <Button variant="danger" type="submit" id="submitBtn" disabled>
                         Submit
                     </Button>
                 }
             </Form>
                   
        )
       
}
