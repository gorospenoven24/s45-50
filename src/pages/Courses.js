import { Fragment, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';

export default function Courses(){
	// console.log(coursesData)
	// console.log(coursesData[0])
	//  to get all the courses in coursecard

// State that will be use to store the course retrieve from the database
// useEffect is a use to fetch data

const [courses, setCourses] = useState([]);
	useEffect(()=>{
		fetch('http://localhost:4000/courses/all')
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			// set the courses state to map the data retrieved from the fetch request in several "Courescard"
			setCourses(data.map(course => {
					return (
						// key is to get the id of coursesData
						< CourseCard key={course.id} courseProp={course}/>
					)
				})
			);
			
		})
	}, [])

	return (
			<Fragment>
				{courses}
			</Fragment>
		
	)
}