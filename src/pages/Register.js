import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';
import Courses from './Courses';
import Swal from 'sweetalert2';

export default function Register(){
	// add codes for activity------------------------
	  const {user, setUser} = useContext(UserContext)
	 // end ---------------------


	// state hooks to store the value to input fields
	const [email,setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo,setMobileNo] = useState('');
	// const [isAdmin,setisAdmin] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// state to determine wether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	//  function to simulate user registration
		function registerUser(e){
			 // Prevents page redirection via form submission
	        e.preventDefault();

       fetch('http://localhost:4000/users/checkEmail', {
                method: 'POST',
                headers:{
                    'Content-Type':'application/json'
                },
                body: JSON.stringify({
                     email:email
                  
                })
            })
            .then(res => res.json())
            .then(data => {
                if(data === false){
                	fetch('http://localhost:4000/users/register', {
                		method: 'POST',
                		headers:{
                		    'Content-Type':'application/json'
                		},
                		body: JSON.stringify({
                			firstName:firstName,
                			lastName:lastName,
                		     email:email,
                		     password: password1,
                		     mobileNo:mobileNo
                		  
                		})

                	})
                	.then(res => res.json())
                	.then(data => {
                		console.log(`${data} dataDATA`);
                		Swal.fire({
                			title:"registration Successfully",
                			icon:"success",
                			text:"welcome to zuitt"
                		});
                		<Redirect to ="/login"/>
                	})

                }
                else{
                	Swal.fire({
                			title:"Email is already exist",
                			icon:"error",
                			text: "Check your Email details and try again."
                		});
               		 }

            })


		
			setFirstName('');
			setLastName('');
			setEmail('');
			setMobileNo('');
			setPassword1('');
			setPassword2('');

			// alert('Thankyou for registering')
		}

	useEffect(() =>{
		// validate to enable submit button when all fiels are populated and both passwords are match

		if ((firstName !== ''  && lastName !== ''  && email !== ''  &&  mobileNo !== ''  &&  password1 !== '' && password2 !== '') && ( password1 === password2  ))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[firstName,lastName, mobileNo,email, password1,password2])

	

	return (
		// add codes for activity----------------------------
			 (user.id !== null) ?
               <Redirect to="/courses"/>
             :
             // end ---------------------


			<Form onSubmit = {(e)=> registerUser(e)}>
		{/*Bind input states via 2 way binding*/}
		 <Form.Group controlId="userEmail">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter FirstName" 
			    	value={firstName}
			    	onChange = { e => setFirstName(e.target.value)}
			    	required 
			    	/>
			    <Form.Text className="text-muted">
			    
			    </Form.Text>
			  </Form.Group>
			   <Form.Group controlId="userEmail">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter lastName" 
			    	value={lastName}
			    	onChange = { e => setLastName(e.target.value)}
			    	required 
			    	/>
			    <Form.Text className="text-muted">
			   
			    </Form.Text>
			  </Form.Group>
			  <Form.Group controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value={email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    	/>
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>
			   <Form.Group controlId="userEmail">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter Mobile number" 
			    	value={mobileNo}
			    	onChange = { e => setMobileNo(e.target.value)}
			    	required 
			    	minlength = "11"
			    	/>
			    <Form.Text className="text-muted">
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control
			    	 type="password" 
			    	 placeholder="Password" 
			    	 value = {password1}
			    	 onChange = {e => setPassword1(e.target.value)}
			    	 required
			    	 />
			  </Form.Group>
			  <Form.Group controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value = {password2}
			    	onChange = {e => setPassword2(e.target.value)}
			    	required 
			    	 />
			  </Form.Group>
			{/* conditionaly render the submit button based on isActive state*/}
			{/*ternary operator*/}
				{ isActive ?
					  <Button variant="primary" type="submit" id="submitBtn">
					    Submit
					  </Button>
			 	 :
			   		 <Button variant="danger" type="submit" id="submitBtn" disabled>
			   		 Submit
			 	 	</Button>

				 }
			
			</Form>
	)
}
