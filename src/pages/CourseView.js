import { useState } from 'react';
import {Container , Card, Button, Row, Col} from 'react-bootstrap';

export default function CourseView(){

 	const [name,setName] =  useState("")
 	const [description,setDescription]  =  useState("")
 	const [price, setPrice]=  useState("0")	


 	return(

 		<Container  className = "mt-5">
 			<Col>
 				<Card>
	 				<Card.Body className ="text-center">
	 					<Card.Title>{name}</Card.Title>
	 					<Card.Text>Description</Card.Text>
	 					<Card.Subtitle>{description}</Card.Subtitle>
	 					<Card.Text>Price</Card.Text>
	 					<Card.Subtitle>{price}</Card.Subtitle>
	 					<Card.Text>claass Schedule</Card.Text>
	 					<Card.Subtitle></Card.Subtitle>
	 					<Card.Text>8:00am - 5;00 pm</Card.Text>
	 					<Button	variant="primary" block>Enroll</Button>
	 				</Card.Body>
 				</Card>
 			</Col>

 		</Container> 
 		)
}